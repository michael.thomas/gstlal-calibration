#!/usr/bin/env python3
#
# Copyright (C) 2023  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import numpy as np
import time
import matplotlib.patches as mpatches
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 20
matplotlib.rcParams['legend.fontsize'] = 20
matplotlib.rcParams['mathtext.default'] = 'regular'
import matplotlib.pyplot as plt

from optparse import OptionParser, Option

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments
from gstlal import test_common

from gstlal import FIRtools as fir
from ticks_and_grid import ticks_and_grid

parser = OptionParser()
parser.add_option("--gps-start-time", metavar = "seconds", type = int, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", type = str, help = "Name of the interferometer (IFO), e.g., H1, L1")
parser.add_option("--frame-cache", metavar = "name", type = str, help = "Frame cache file that contains data")
parser.add_option("--state-vector-channel", metavar = "name", type = str, default = "GDS-CALIB_STATE_VECTOR", help = "Channel-name of state vector")
parser.add_option("--sample-rate", metavar = "Hz", type = int, default = 16, help = "Sample rate of state vector data.")
parser.add_option("--bits", metavar = "list", type = str, default = "HOFT_OK,OBS_INTENT,LOW_NOISE,FILTERS_OK,NO_GAP,NO_STOCH_INJ,NO_CBC_INJ,NO_BURST_INJ,NO_DETCHAR_INJ,UNDISTURBED_OK,KTST_OK,KPUM_OK,KUIM_OK,KC_OK,FCC_OK,FS_OK,SRC_Q_OK,LINE_SUB_OK,NOISE_SUB_OK,NOISE_SUB_GATE,NONSENS_SUB_OK", help = "Comma-separated list of names for the bits of the state vector.  The length of this list is the assumed number of bits.")

options, filenames = parser.parse_args()

# Get the options
gps_start_time = options.gps_start_time
gps_end_time = options.gps_end_time
ifo = options.ifo
frame_cache = options.frame_cache
state_vector_channel = options.state_vector_channel
channel_list = [(ifo, state_vector_channel)]
sample_rate = options.sample_rate
bits = options.bits.split(",")
N_bits = len(bits)

# 
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


#
# This pipeline reads in time series data and writes it to file.
#


def read_data(pipeline, name):

	data = pipeparts.mklalcachesrc(pipeline, location = frame_cache, cache_dsc_regex = ifo)
	#data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
	data = pipeparts.mkframecppchanneldemux(pipeline, data, do_file_checksum = False, skip_bad_files = True)
	data = calibration_parts.hook_up(pipeline, data, state_vector_channel, ifo, 1.0)
	#data = calibration_parts.caps_and_progress(pipeline, data, "audio/x-raw,format=U32LE,channel-mask=(bitmask)0x0", "calib_statevector")
	#data = calibration_parts.mkresample(pipeline, data, 0, False, sample_rate)
	pipeparts.mknxydumpsink(pipeline, data, '%s_state_vector_data_%d-%d.txt' % (ifo, gps_start_time, gps_end_time - gps_start_time))

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


# Run pipeline
test_common.build_and_run(read_data, "read_data", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * gps_start_time), LIGOTimeGPS(0, 1000000000 * gps_end_time))))

statevector = np.loadtxt('%s_state_vector_data_%d-%d.txt' % (ifo, gps_start_time, gps_end_time - gps_start_time))

t = np.transpose(statevector)[0]
t_start = t[0]
t -= t_start
t = t[::sample_rate]
dur = t[-1]
t_unit = 'seconds'
sec_per_t_unit = 1.0
if dur > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif dur > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif dur > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0
t /= sec_per_t_unit

t5 = int(round(t[-1] / 5))
tier = int(np.round(3*np.log10(t5)))
t_tick_space = 10**(tier // 3) * int(round(10**((tier % 3) / 3)))
t_ticks = np.arange(0, t[-1], t_tick_space)
t_tick_locations = t_ticks * sec_per_t_unit

statevector = np.transpose(statevector)[1]

bit_values = np.ones((N_bits, len(t)), dtype = int)
for i in range(len(statevector)):
	sv = bin(int(round(statevector[i])))
	if len(sv) - 2 > N_bits:
		raise ValueError("Found %d bits, but only expected %d bits" % (len(sv) - 2, N_bits))
	for j in range(len(sv) - 2):
		bit_values[j][i // sample_rate] &= int(sv[len(sv) - 1 - j])
	# The rest of the bits are zero
	for j in range(len(sv) - 2, N_bits):
		bit_values[j][i // sample_rate] = 0

# Color of the bars on the state vector plot
on_color = 'green'
off_color = 'red'

# Make plots
fig, axs = plt.subplots(N_bits, figsize = (15, 8))
fig.suptitle("%s Calibration State" % ifo)

for i in range(N_bits):
	colors = [(on_color if j > 0.9 else off_color) for j in bit_values[i]]

	print("%s" % bits[i])
	for j, k in enumerate(bit_values[i]):
		axs[i].barh(y = 0, width = 1, height = (k + 2) / 3, left = j, color = colors[j])
	axs[i].barh(y = 0, width = 1, height = 1, left = len(bit_values[i]), color = 'black')
	plt.sca(axs[i])
	if i < N_bits - 1:
		plt.xticks(ticks = t_tick_locations)
		axs[i].xaxis.set_tick_params(labelbottom=False)
	axs[i].yaxis.set_tick_params(left=False)
	plt.yticks(ticks = [0, 0.25, 0.5], labels = [bits[i], '', ''])

plt.xticks(ticks = t_tick_locations, labels = t_ticks)
plt.xlabel("Time in %s since %s UTC" % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))

#fig.tight_layout(rect = [0.2, 0, 1, 1])
#fig.tight_layout()
fig.subplots_adjust(left = 0.2)

plt.savefig('%s_state_vector_plot_%d-%d.png' % (ifo, gps_start_time, gps_end_time - gps_start_time))


