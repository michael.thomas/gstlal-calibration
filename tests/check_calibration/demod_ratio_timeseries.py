#!/usr/bin/env python3
# Copyright (C) 2018  Aaron Viets
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#
# =============================================================================
#
#				   Preamble
#
# =============================================================================
#


import matplotlib; matplotlib.use('Agg')
import sys
import os
import numpy
import time
from math import pi
import resource
import datetime
import time
from matplotlib import rc
rc('text', usetex = True)
matplotlib.rcParams['font.family'] = 'Times New Roman'
matplotlib.rcParams['font.size'] = 22
matplotlib.rcParams['legend.fontsize'] = 18
matplotlib.rcParams['mathtext.default'] = 'regular'
import glob
import matplotlib.pyplot as plt

from matplotlib import cm
from ticks_and_grid import ticks_and_grid

from optparse import OptionParser, Option

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
Gst.init(None)

import lal
from lal import LIGOTimeGPS

from gstlal import pipeparts
from gstlalcalibration import calibration_parts
from gstlal import test_common
from gstlal import simplehandler
from gstlal import datasource

from ligo import segments

parser = OptionParser()
parser.add_option("--gps-start-time", metavar = "seconds", type = int, help = "GPS time at which to start processing data")
parser.add_option("--gps-end-time", metavar = "seconds", type = int, help = "GPS time at which to stop processing data")
parser.add_option("--ifo", metavar = "name", type = str, help = "Name of the interferometer (IFO), e.g., H1, L1")
parser.add_option("--denominator-frame-cache", metavar = "name", type = str, help = "Frame cache file that contains denominator")
parser.add_option("--numerator-frame-cache", metavar = "name", type = str, help = "Frame cache file that contains numerator")
parser.add_option("--denominator-channel-name", metavar = "name", type = str, default = "CAL-PCALY_TX_PD_OUT_DQ", help = "Channel-name of denominator")
parser.add_option("--numerator-channel-name", metavar = "name", type = str, default = None, help = "Channel-name of numerator")
parser.add_option("--frequencies", metavar = "list", type = str, help = "List of frequencies at which to take ratios. Semicolons separate frequencies to be put on separate plots, and commas separate frequencies to be put on the same plot.")
parser.add_option("--filter-time", metavar = "seconds", type = int, default = 40, help = "Length in seconds of the low-pass filter used for demodulation")
parser.add_option("--average-time", metavar = "seconds", type = int, default = 128, help = "Length in seconds of the running average applied to the ratio")
parser.add_option("--magnitude-ranges", metavar = "list", type = str, help = "List of limits for magnitude plots. Semicolons separate ranges for different plots, and commas separate the min and max of a single plot.")
parser.add_option("--phase-ranges", metavar = "list", type = str, help = "List of limits for phase plots. Semicolons separate ranges for different plots, and commas separate the min and max of a single plot.")
parser.add_option("--plot-titles", metavar = "names", type = str, help = "Semicolon-separated list of titles for plots")
parser.add_option("--show-stats", action = "store_true", help = "If set, averages and standard deviations will be shown in the plot legends.")

options, filenames = parser.parse_args()

ifo = options.ifo

# Set up channel list
channel_list = [(ifo, options.denominator_channel_name), (ifo, options.numerator_channel_name)]

# Convert the list of frequencies to a list of floats
freq_list = options.frequencies.split(';')
frequencies = []
for i in range(0, len(freq_list)):
	freq_list[i] = freq_list[i].split(',')
	for j in range(0, len(freq_list[i])):
		freq_list[i][j] = float(freq_list[i][j])
		frequencies.append(freq_list[i][j])

# demodulation and averaging parameters
filter_time = options.filter_time
average_time = options.average_time
rate_out = 1

#
# =============================================================================
#
#				  Pipelines
#
# =============================================================================
#


def demod_ratio(pipeline, name):

	# Get denominator data from the raw frames
	denominator_data = pipeparts.mklalcachesrc(pipeline, location = options.denominator_frame_cache, cache_dsc_regex = ifo)
	denominator_data = pipeparts.mkframecppchanneldemux(pipeline, denominator_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
	denominator = calibration_parts.hook_up(pipeline, denominator_data, options.denominator_channel_name, ifo, 1.0)
	denominator = calibration_parts.caps_and_progress(pipeline, denominator, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "denominator")
	denominator = pipeparts.mktee(pipeline, denominator)

	# Get numerator data from the raw frames
	if not options.denominator_frame_cache == options.numerator_frame_cache:
		numerator_data = pipeparts.mklalcachesrc(pipeline, location = options.numerator_frame_cache, cache_dsc_regex = ifo)
		numerator_data = pipeparts.mkframecppchanneldemux(pipeline, numerator_data, do_file_checksum = False, skip_bad_files = True, channel_list = list(map("%s:%s".__mod__, channel_list)))
		numerator = calibration_parts.hook_up(pipeline, numerator_data, options.numerator_channel_name, ifo, 1.0)
	else:
		numerator = calibration_parts.hook_up(pipeline, denominator_data, options.numerator_channel_name, ifo, 1.0)
	numerator = calibration_parts.caps_and_progress(pipeline, numerator, "audio/x-raw,format=F64LE,channels=1,channel-mask=(bitmask)0x0", "numerator")
	numerator = pipeparts.mktee(pipeline, numerator)

	# Demodulate numerator and denominator at each frequency, take ratios, and write to file
	for i in range(0, len(frequencies)):
		# Demodulate
		demodulated_denominator = calibration_parts.demodulate(pipeline, denominator, frequencies[i], True, rate_out, filter_time, 0.5)
		demodulated_numerator = calibration_parts.demodulate(pipeline, numerator, frequencies[i], True, rate_out, filter_time, 0.5)
		demodulated_numerator = pipeparts.mktee(pipeline, demodulated_numerator)
		demodulated_denominator = pipeparts.mktee(pipeline, demodulated_denominator)
		# Take ratio
		ratio = calibration_parts.complex_division(pipeline, demodulated_numerator, demodulated_denominator)
		# Average
		ratio = pipeparts.mkgeneric(pipeline, ratio, "lal_smoothkappas", array_size = 1, avg_array_size = int(rate_out * average_time), filter_latency = 1.0)
		# Find magnitude and phase
		ratio = pipeparts.mktee(pipeline, ratio)
		magnitude = pipeparts.mkgeneric(pipeline, ratio, "cabs")
		phase = pipeparts.mkgeneric(pipeline, ratio, "carg")
		phase = pipeparts.mkaudioamplify(pipeline, phase, 180.0 / numpy.pi)
		# Interleave
		magnitude_and_phase = calibration_parts.mkinterleave(pipeline, [magnitude, phase])
		# Write to file
		pipeparts.mknxydumpsink(pipeline, magnitude_and_phase, "%s_%s_over_%s_%0.1fHz.txt" % (ifo, options.numerator_channel_name, options.denominator_channel_name, frequencies[i]))

	#
	# done
	#

	return pipeline

#
# =============================================================================
#
#				     Main
#
# =============================================================================
#


# Run pipeline
test_common.build_and_run(demod_ratio, "demod_ratio", segment = segments.segment((LIGOTimeGPS(0, 1000000000 * options.gps_start_time), LIGOTimeGPS(0, 1000000000 * options.gps_end_time))))

# Read data from files and plot it
colors = ['blueviolet', 'darkgreen', 'limegreen', 'khaki', 'black', 'dimgray', 'violet', 'coral', 'royalblue', 'magenta', 'cyan', 'red', 'chocolate', 'gold', 'orange', 'pink', 'greenyellow', 'lightgray', 'yellow', 'hotpink'] # Hopefully the user will not want to plot more than six datasets on one plot.
for i in range(0, len(freq_list)):
	data = numpy.loadtxt("%s_%s_over_%s_%0.1fHz.txt" % (ifo, options.numerator_channel_name, options.denominator_channel_name, freq_list[i][0]))
	t_start = data[0][0]
	dur = data[len(data) - 1][0] - t_start
	t_unit = 'seconds'
	sec_per_t_unit = 1.0
	if dur > 60 * 60 * 100:
		t_unit = 'days'
		sec_per_t_unit = 60.0 * 60.0 * 24.0
	elif dur > 60 * 100:
		t_unit = 'hours'
		sec_per_t_unit = 60.0 * 60.0
	elif dur > 100:
		t_unit = 'minutes'
		sec_per_t_unit = 60.0
	times = []
	magnitudes = [[]]
	phases = [[]]
	for k in range(0, int(len(data) / (filter_time + average_time))):
		times.append((data[(filter_time + average_time) * k][0] - t_start) / sec_per_t_unit)
		magnitudes[0].append(data[(filter_time + average_time) * k][1])
		phases[0].append(data[(filter_time + average_time) * k][2])
	markersize = 150.0 * numpy.sqrt(float((filter_time + average_time) / dur))
	markersize = min(markersize, 10.0)
	markersize = max(markersize, 0.2)
	# Make plots
	plt.figure(figsize = (10, 10))
	plt.subplot(211)
	if options.show_stats:
		plt.plot(times, magnitudes[0], colors[0], linestyle = 'None', marker = '.', markersize = markersize, label = r'${\rm %0.1f \ Hz} \ [\mu = %0.4f, \sigma = %0.4f]$' % (freq_list[i][0], numpy.mean(magnitudes[0]), numpy.std(magnitudes[0])))
	else:
		plt.plot(times, magnitudes[0], colors[0], linestyle = 'None', marker = '.', markersize = markersize)#, label = r'${\rm %0.1f \ Hz}$' % (freq_list[i][0]))
	#plt.title(options.plot_titles.split(';')[i])
	plt.ylabel(r'${\rm Magnitude}$')
	if options.magnitude_ranges is None:
		magnitude_range = '0.8,1.2'
	else:
		magnitude_range = options.magnitude_ranges.split(';')[i]
	plt.ylim(float(magnitude_range.split(',')[0]), float(magnitude_range.split(',')[1]))
	plt.grid(True)
	#leg = plt.legend(fancybox = True, markerscale = 8.0 / markersize, numpoints = 3)
	#leg.get_frame().set_alpha(0.8)
	plt.subplot(212)
	if options.show_stats:
		plt.plot(times, phases[0], colors[0], linestyle = 'None', marker = '.',  markersize = markersize, label = r'${\rm %0.1f \ Hz} \ [\mu = %0.1f^{\circ}, \sigma = %0.1f^{\circ}]$' % (freq_list[i][0], numpy.mean(phases[0]), numpy.std(phases[0])))
	else:
		plt.plot(times, phases[0], colors[0], linestyle = 'None', marker = '.',  markersize = markersize, label = r'${\rm %0.1f \ Hz}$' % (freq_list[i][0]))
	plt.ylabel(r'${\rm Phase \ [deg]}$')
	plt.xlabel(r'${\rm Time \ in \ %s \ since \ %s \ UTC}$' % (t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))
	if options.phase_ranges is None:
		phase_range = '-20,20'
	else:
		phase_range = options.phase_ranges.split(';')[i]
	plt.ylim(float(phase_range.split(',')[0]), float(phase_range.split(',')[1]))
	plt.grid(True)
	leg = plt.legend(fancybox = True, markerscale = 8.0 / markersize, numpoints = 3)
	leg.get_frame().set_alpha(0.8)
	for j in range(1, len(freq_list[i])):
		data = numpy.loadtxt("%s_%s_over_%s_%0.1fHz.txt" % (ifo, options.numerator_channel_name, options.denominator_channel_name, freq_list[i][j]))
		magnitudes.append([])
		phases.append([])
		for k in range(0, int(len(data) / (filter_time + average_time))):
			magnitudes[j].append(data[(filter_time + average_time) * k][1])
			phases[j].append(data[(filter_time + average_time) * k][2])
		plt.subplot(211)
		if options.show_stats:
			plt.plot(times, magnitudes[j], colors[j], linestyle = 'None', marker = '.', markersize = markersize, label = r'${\rm %0.1f \ Hz} \ [\mu = %0.4f, \sigma = %0.4f]$' % (freq_list[i][j], numpy.mean(magnitudes[j]), numpy.std(magnitudes[j])))
		else:
			plt.plot(times, magnitudes[j], colors[j], linestyle = 'None', marker = '.', markersize = markersize)#, label = r'${\rm %0.1f \ Hz}$' % (freq_list[i][j]))
		#leg = plt.legend(fancybox = True, markerscale = 8.0 / markersize, numpoints = 3)
		#leg.get_frame().set_alpha(0.8)
		plt.subplot(212)
		if options.show_stats:
			plt.plot(times, phases[j], colors[j], linestyle = 'None', marker = '.', markersize = markersize, label = r'${\rm %0.1f \ Hz} \ [\mu = %0.1f^{\circ}, \sigma = %0.1f^{\circ}]$' % (freq_list[i][j], numpy.mean(phases[j]), numpy.std(phases[j])))
		else:
			plt.plot(times, phases[j], colors[j], linestyle = 'None', marker = '.', markersize = markersize, label = r'${\rm %0.1f \ Hz}$' % (freq_list[i][j]))
		leg = plt.legend(fancybox = True, markerscale = 8.0 / markersize, numpoints = 3)
		leg.get_frame().set_alpha(0.8)
	if options.plot_titles is None:
		plt.savefig('demod_ratio_%d-%d.png' % (int(t_start), int(dur)))
		plt.savefig('demod_ratio_%d-%d.pdf' % (int(t_start), int(dur)))
	else:
		plt.savefig('%s_%d-%d.png' % (options.plot_titles.split(';')[i].replace(' ', '_'), int(t_start), int(dur)))
		plt.savefig('%s_%d-%d.pdf' % (options.plot_titles.split(';')[i].replace(' ', '_'), int(t_start), int(dur)))

# Now, a figure with plots of all frequencies on one curve, temporal evolution shown as color
frequencies = numpy.asarray(frequencies)
frequencies.sort()

data = numpy.loadtxt("%s_%s_over_%s_%0.1fHz.txt" % (ifo, options.numerator_channel_name, options.denominator_channel_name, frequencies[0]))
t_start = data[0][0]
dur = data[len(data) - 1][0] - t_start
t_unit = 'seconds'
sec_per_t_unit = 1.0
if dur > 60 * 60 * 100:
	t_unit = 'days'
	sec_per_t_unit = 60.0 * 60.0 * 24.0
elif dur > 60 * 100:
	t_unit = 'hours'
	sec_per_t_unit = 60.0 * 60.0
elif dur > 100:
	t_unit = 'minutes'
	sec_per_t_unit = 60.0

num_curves = int(len(data) / (filter_time + average_time))
colors = [cm.viridis(x) for x in numpy.linspace(0, 1, num_curves)]
times = []
magnitudes = []
phases = []
for i in range(num_curves):
	times.append((data[(filter_time + average_time) * i][0] - t_start) / sec_per_t_unit)
	magnitudes.append([])
	phases.append([])

for f in frequencies:
	data = numpy.loadtxt("%s_%s_over_%s_%0.1fHz.txt" % (ifo, options.numerator_channel_name, options.denominator_channel_name, f))
	for i in range(num_curves):
		magnitudes[i].append(data[(filter_time + average_time) * i][1])
		phases[i].append(data[(filter_time + average_time) * i][2])

fig = plt.figure(figsize = (20, 15))
fig.suptitle("TF Evolution over %0.1f %s starting %s UTC: Blue = Start; Yellow = Thermalized" % (times[-1], t_unit, time.strftime("%b %d %Y %H:%M:%S".replace(':', '{:}').replace('-', '\mbox{-}').replace(' ', '\ '), time.gmtime(t_start + 315964782))))

magnitude_max = 0
magnitude_min = numpy.inf
for i in range(len(freq_list)):
	if options.magnitude_ranges is None:
		magnitude_range = '0.8,1.2'
	else:
		magnitude_range = options.magnitude_ranges.split(';')[i]
	if float(magnitude_range.split(',')[0]) < magnitude_min:
		magnitude_min = float(magnitude_range.split(',')[0])
	if float(magnitude_range.split(',')[1]) > magnitude_max:
		magnitude_max = float(magnitude_range.split(',')[1])

phase_max = -180
phase_min = 180
for i in range(len(freq_list)):
	if options.phase_ranges is None:
		phase_range = '-20,20'
	else:
		phase_range = options.phase_ranges.split(';')[i]
	if float(phase_range.split(',')[0]) < phase_min:
		phase_min = float(phase_range.split(',')[0])
	if float(phase_range.split(',')[1]) > phase_max:
		phase_max = float(phase_range.split(',')[1])

# Magnitude plots
plt.subplot(2, 1, 1)
for j in range(num_curves):
	# Plot magnitude for each time
	plt.plot(frequencies, magnitudes[j], color = colors[j], linewidth = 1.0)
	if j == num_curves - 1:
		ticks_and_grid(plt.gca(), ymin = magnitude_min, ymax = magnitude_max, xscale = 'log', yscale = 'log')
		plt.ylabel("Magnitude")

# Phase plots
plt.subplot(2, 1, 2)
for j in range(num_curves):
	# Plot phase for each time
	plt.plot(frequencies, phases[j], color = colors[j], linewidth = 1.0)
	if j == num_curves - 1:
		ticks_and_grid(plt.gca(), ymin = phase_min, ymax = phase_max, xscale = 'log')
		plt.xlabel("Frequency (Hz)")
		plt.ylabel("Phase")

if options.plot_titles is None:
	plt.savefig('demod_ratio_TFevolution_%d-%d.png' % (int(t_start), int(dur)))
	plt.savefig('demod_ratio_TFevolution_%d-%d.pdf' % (int(t_start), int(dur)))
else:
	plt.savefig('%s_TFevolution_%d-%d.png' % (options.plot_titles.split(';')[i].replace(' ', '_'), int(t_start), int(dur)))
	plt.savefig('%s_TFevolution_%d-%d.pdf' % (options.plot_titles.split(';')[i].replace(' ', '_'), int(t_start), int(dur)))


